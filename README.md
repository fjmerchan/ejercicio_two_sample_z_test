# Taller 4: Two-sample z-test
Este repositorio contiene el código necesario en R para empezar con un análisis estadístico de los resultados de 1543 grabaciones realizadas en el sistema RAP.

* load_data.R: Carga los datos a frames de R
* analisis.R: Analisis de los datos usando el test two-sample z-test
* reporte-14.xlsx: Excel con datos anonimizados de 1543 grabaciones en el sistema RAP durante el primer semestre del 2018. 

