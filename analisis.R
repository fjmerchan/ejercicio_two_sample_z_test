source('load_data.R')

grupo_retro <- group_by(estudiantes_2_intentos_grupos, Retroalimentacion)
estadisticos <- summarize(grupo_retro, Promedio_Global = mean(Calificacion_global), SD_Global = sd(Calificacion_global))
View(estadisticos)

# Incluir aquí, analisis two-sample z-test


###############################################
# Definición de Hipotesis
################################################

# Ho=  Se espera que los promedios de las dos muestras sean iguales,y que
#      la diferencia observada es simplemente consecuencia de la variabilidad aleatoria.

# Ha = La diferencia observada es real causada por la retroalmentación realizada por el profesor.

###############################
# Cálculos de los SE
# SE= SDMuestra/sqrt(n)
###############################

SE_Profesor=estadisticos$SD_Global[1]/sqrt(55)
print (SE_Profesor)
SE_Sistema=estadisticos$SD_Global[2]/sqrt(55)
print (SE_Sistema)


###############################
# Cálculos de los SE Diferencia
# SE= sqrt(SEa^2 +SEB^2)
###############################

SE_Diferencia= sqrt(SE_Profesor^2 +SE_Sistema^2)
print (SE_Diferencia)


###############################
# Cálculo de  Z
# SE= observado-esperado/SEDiferencia
###############################

z= (estadisticos$Promedio_Global[1] - estadisticos$Promedio_Global[2])/SE_Diferencia
print(z)


###############################
# Cálculo de  P
###############################

p_value = 1 - pnorm(z)
print("El valor de p es:")
print(p_value)


#######################################
#
# Análisis
#
########################################

# La retroalmientación que brinda el profesor significamente no es mejor que la  retroalimentación
# del sistema, esto se puede demostrar debido al valor que tiene p_value es de 0.1876555, 
# Por lo que se da como valida la nulla "H0", es decir que de igual quien realice la retroalimentación 